#ifndef KL_GRID_HAS_BEEN_INCLUDED
#define KL_GRID_HAS_BEEN_INCLUDED

#include "klAccessor.h"
#include <FabricEDK.h>

#include <openvdb/openvdb.h>
#include "openvdb/tools/LevelSetSphere.h"
#include "openvdb/tools/ChangeBackground.h"
#include "openvdb/tools/Prune.h"
#include "openvdb/tools/SignedFloodFill.h"


using namespace Fabric::EDK;

namespace klTypes{

/// Wrapper for iterator classes
template<typename _GridT, typename _IterT>
class IterWrap
{
public:
  typedef _GridT                              GridT;
  typedef GridT                               NonConstGridT;
  typedef typename NonConstGridT::Ptr         GridPtrT;
  typedef _IterT IterT;
  typedef typename GridT::ValueType ValueT;

  IterWrap(IterWrap& other): mIter( other.iter() ){}
  IterWrap(const IterT& iter): mIter(iter) {}

  // GridPtrT parent() { return mGrid; }

  IterT iter() { return mIter; }

  IterWrap copy() { return *this; }

  bool test() const { return mIter.test(); }

  void next()
  {
    if (!mIter)
      throwException("no more values");
    ++mIter;
  }
private:
  // To keep this iterator's grid from being deleted, leaving the iterator dangling,
  // store a shared pointer to the grid.
  // GridPtrT mGrid;
  IterT mIter;
}; // class IterWrap


// /// Wrapper for iterator classes
// template<typename _GridT, typename _IterT>
// class IterWrap
// {
// public:
//   typedef _GridT                              GridT;
//   typedef GridT                               NonConstGridT;
//   typedef typename NonConstGridT::Ptr         GridPtrT;
//   typedef _IterT IterT;
//   typedef typename GridT::ValueType ValueT;

//   IterWrap(IterWrap& other):mGrid( other.parent() ), mIter( other.iter() ){}
//   IterWrap(GridPtrT grid, const IterT& iter): mGrid(grid), mIter(iter) {}

//   GridPtrT parent() { return mGrid; }

//   IterT iter() { return mIter; }

//   IterWrap copy() { return *this; }

//   bool test() const { return mIter.test(); }

//   void next()
//   {
//     if (!mIter)
//       throwException("no more values");
//     ++mIter;
//   }
// private:
//   // To keep this iterator's grid from being deleted, leaving the iterator dangling,
//   // store a shared pointer to the grid.
//   GridPtrT mGrid;
//   IterT mIter;
// }; // class IterWrap

// /// Wrapper for iterator classes
// template<typename _GridT, typename _IterT>
// class IterWrap<const _GridT, _IterT>
// {
// public:
//   typedef const _GridT                            GridT;
//   typedef _GridT                                  NonConstGridT;
//   typedef typename NonConstGridT::ConstPtr        GridPtrT;
//   typedef _IterT IterT;
//   typedef typename GridT::ValueType ValueT;

//   IterWrap(IterWrap& other):mGrid( other.parent() ), mIter( other.iter() ){}
//   IterWrap(GridPtrT grid, const IterT& iter): mGrid(grid), mIter(iter) {}

//   GridPtrT parent() const { return mGrid; }

//   IterT iter() const { return mIter; }

//   IterWrap copy() { return *this; }

//   bool test() const { return mIter.test(); }

//   void next()
//   {
//     if (!mIter)
//       throwException("no more values");
//     ++mIter;
//   }
// private:
//   // To keep this iterator's grid from being deleted, leaving the iterator dangling,
//   // store a shared pointer to the grid.
//   GridPtrT mGrid;
//   IterT mIter;
// }; // class IterWrap

template<typename GridType>
class klGridTyped
{
  typedef typename GridType::ValueType ValueT;
  typedef typename GridType::Ptr GridPtr;

public:
  typedef GridType Type;
  typedef GridPtr Ptr;
  
  klGridTyped()
   : mGrid( Type::create(openvdb::zeroVal<ValueT>()) ),
     mOwned(true)
  {  AtomicUInt32SetValue( 1, &mRefCount );  }

  klGridTyped(ValueT background, bool owned = false)
   : mGrid( Type::create(background) ),
     mOwned(owned)
  {  AtomicUInt32SetValue( 1, &mRefCount );  }

  klGridTyped(const Ptr& other, bool owned = false)
   : mGrid(other),
     mOwned(owned)
  {  AtomicUInt32SetValue( 1, &mRefCount );  }

  klGridTyped(const Ptr& other, openvdb::ShallowCopy shallow, bool owned = false)
   : mGrid( new Type(*other,shallow) ),
     mOwned(owned)
  {    AtomicUInt32SetValue( 1, &mRefCount );  }

  void retain()
  {    AtomicUInt32Increment( &mRefCount );  }

  void release()
  {
    if ( AtomicUInt32DecrementAndGetValue( &mRefCount ) == 0 )delete this;
  }

  void setData(Ptr& data)
  {
    mGrid = data;
  }

  void setOwned(bool owned)
  {
    mOwned = owned;
  }

  Ptr& data()
  {
    return mGrid;
  }

  const Ptr& data() const
  {
    return mGrid;
  }
private:
  Ptr mGrid;
  bool mOwned;
  uint32_t mRefCount;
}; // class klGridTyped

template<typename GridType>
typename GridType::Ptr& operator*(klGridTyped<GridType>& grid){
  return grid.data() ;
}

template <typename T>
class klBasePtr
{
public:
  typedef T Type;
  typedef typename Type::Ptr Ptr;

  klBasePtr(const Ptr& other, bool owned = false)
   : m_data(other),
     m_owned(owned)
  {  AtomicUInt32SetValue( 1, &mRefCount );  }

  klBasePtr()
   : m_data(),
     m_owned(true)
  {  AtomicUInt32SetValue( 1, &mRefCount );  }

  void retain()
  {
    AtomicUInt32Increment( &mRefCount );
  }

  void release()
  {
    if ( AtomicUInt32DecrementAndGetValue( &mRefCount ) == 0 )delete this;
  }

  void setData(Ptr& data)
  {
    m_data = data;
  }

  void setOwned(bool owned)
  {
    m_owned = owned;
  }

  Ptr& data()
  {
    return m_data;
  }

  const Ptr& data() const
  {
    return m_data;
  }
private:
  Ptr m_data;
  bool m_owned;
  uint32_t mRefCount;
 }; // class klBasePtr

}//ending klTypes namespace

#endif // KL_GRID_HAS_BEEN_INCLUDED
interface TreeInterface {
  /// Return the name of this tree's type.

  /// Return the name of the type of a voxel's value (e.g., "float" or "vec3d").
  String valueType?();

  /// Return a pointer to a deep copy of this tree
  // Ref<Object> copy?();

  //
  // Tree methods
  //
  /// @brief Return this tree's background value wrapped as metadata.
  /// @note Query the metadata object for the value's type.
  // Metadata getBackgroundValue?();
  // const { return Metadata(); }

  /// @brief Return in @a bbox the axis-aligned bounding box of all
  /// leaf nodes and active tiles.
  /// @details This is faster then calling evalActiveVoxelBoundingBox,
  /// which visits the individual active voxels, and hence
  /// evalLeafBoundingBox produces a less tight, i.e. approximate, bbox.
  /// @return @c false if the bounding box is empty (in which case
  /// the bbox is set to its default value).
  Boolean evalLeafBoundingBox?(io CoordBBox bbox);

  /// @brief Return in @a dim the dimensions of the axis-aligned bounding box
  /// of all leaf nodes.
  /// @return @c false if the bounding box is empty.
  Boolean evalLeafDim?(io Coord dim);

  /// @brief Return in @a bbox the axis-aligned bounding box of all
  /// active voxels and tiles.
  /// @details This method produces a more accurate, i.e. tighter,
  /// bounding box than evalLeafBoundingBox which is approximate but
  /// faster.
  /// @return @c false if the bounding box is empty (in which case
  /// the bbox is set to its default value).
  Boolean evalActiveVoxelBoundingBox?(io CoordBBox bbox);

  /// @brief Return in @a dim the dimensions of the axis-aligned bounding box of all
  /// active voxels.  This is a tighter bounding box than the leaf node bounding box.
  /// @return @c false if the bounding box is empty.
  Boolean evalActiveVoxelDim?(io Coord dim);

  getIndexRange?(io CoordBBox bbox);


  //
  // Statistics
  //
  /// @brief Return the depth of this tree.
  ///
  /// A tree with only a root node and leaf nodes has depth 2, for example.
  UInt32 treeDepth?();
  /// Return the number of leaf nodes.
  UInt32 leafCount?();
  /// Return the number of non-leaf nodes.
  UInt32 nonLeafCount?();
  /// Return the number of active voxels stored in leaf nodes.
  UInt64 activeLeafVoxelCount?();
  /// Return the number of inactive voxels stored in leaf nodes.
  UInt64 inactiveLeafVoxelCount?();
  /// Return the total number of active voxels.
  UInt64 activeVoxelCount?();
  /// Return the number of inactive voxels within the bounding box of all active voxels.
  UInt64 inactiveVoxelCount?();

  /// Return the total amount of memory in bytes occupied by this tree.
  UInt64 memUsage?();
  // const { return 0; }


//   //
//   // I/O methods
//   //
//   /// @brief Read the tree topology from a stream.
//   ///
//   /// This will read the tree structure and tile values, but not voxel data.
//   readTopology(std::istream, Boolean saveFloatAsHalf = false);
//   /// @brief Write the tree topology to a stream.
//   ///
//   /// This will write the tree structure and tile values, but not voxel data.
//   writeTopology(std::ostream, Boolean saveFloatAsHalf = false);
//   // const;

//   /// Read all data buffers for this tree.
//   readBuffers(std::istream, Boolean saveFloatAsHalf = false) = 0;
//   /// Write out all the data buffers for this tree.
//   writeBuffers(std::ostream, Boolean saveFloatAsHalf = false);

//   /// @brief Print statistics, memory usage and other information about this tree.
//   /// @param os            a stream to which to write textual information
//   /// @param verboseLevel  1: print tree configuration only; 2: include node and
//   ///                      voxel statistics; 3: include memory usage
//   print(std::ostream os = std::cout, int verboseLevel = 1);
//   // const;

// private:
//   // Disallow copying of instances of this class.
//   //TreeBase(const TreeBase other);
//   TreeBase operator=(const TreeBase other);
};

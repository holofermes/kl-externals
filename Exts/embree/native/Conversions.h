#ifndef CONVERSIONS_HAS_BEEN_INCLUDED
#define CONVERSIONS_HAS_BEEN_INCLUDED


#include <FabricEDK.h>
#include "embree2/rtcore.h"

#include "DataPointer.h"
// other includes


using namespace Fabric::EDK;

#ifdef WIN32
# ifdef _DEBUG
#   define new DEBUG_NEW
# endif
#endif

template<class A, class B>
inline bool conv_from_baseType(const A & a, B & b)
{
  b = a;
  return true;
}

template<class B, class A>
inline bool conv_to_baseType(const A & a, B & b)
{
  b = a;
  return true;
}

inline bool conv_from_String(const KL::String & a, std::string & b)
{
  if(a.data() == NULL)
    b = "";
  else
    b = a.data();
  return true;
}

inline bool conv_to_String(const std::string & a, KL::String & b)
{
  if(a.length() == 0)
    b = "";
  else
    b = a.c_str();
  return true;
}





// struct RTCScene{};

// struct RTCRay{};

// struct RTCBounds{};

// Ref<RTCBoundsFunc>


template<class A, class B>
inline bool conv_from_object(const A & a, B * & b)
{
  if(!a.isValid())
    return false;
  B * grid = (B *)a->pointer;
  if(grid == NULL)
    return false;
  b = grid;
  return true;
}

template<class B, class A>
inline bool conv_to_object(const A * a, B & b, bool owned = false)
{
  if(!b.isValid())
    b = B::Create();
  b->pointer = new A((A::Type)a->data());
  return true;
}

template<typename B, typename A>
inline bool conv_to_struct(const A * a, B & b, bool owned = false)
{
  Fabric::EDK::KL::DataWrapper< DataPointer<A> > out( b.pointer );
  if( ((DataPointer<A>*)b.pointer) != NULL )out->release();
  out = new DataPointer<A>((A*)a, owned);
  return true;
}


template<class A, class B>
inline bool conv_from_struct(const A & a, B * & b)
{
  Fabric::EDK::KL::ConstDataWrapper<  DataPointer<B>  > in( a.pointer );
  if (!in)
    return false;
  b = (B *)in->data();
  return true;
}


template<typename B, typename A>
inline bool conv_to_bounds(const A & from, B & to)
{
  to.lower_x = from.lower_x;
  to.lower_y = from.lower_y;
  to.lower_z = from.lower_z;
  to.upper_x = from.upper_x;
  to.upper_y = from.upper_y;
  to.upper_z = from.upper_z;
  return true;
}

  // RTCBounds localBounds_o = NULL;
  // if(!conv_from_bounds<KL::RTCBounds, RTCBounds>(bounds_o, localBounds_o)){

template<class A, class B>
inline bool conv_from_bounds(const A & from, B & to)
{
  to.lower_x = from.lower_x;
  to.lower_y = from.lower_y;
  to.lower_z = from.lower_z;
  to.upper_x = from.upper_x;
  to.upper_y = from.upper_y;
  to.upper_z = from.upper_z;
  return true;
}



template<class A, class B>
inline bool conv_from_ray(const A & from, B & to)
{

  to.org[0] = from.org.x;
  to.org[1] = from.org.y;
  to.org[2] = from.org.z;
  to.dir[0] = from.dir.x;
  to.dir[1] = from.dir.y;
  to.dir[2] = from.dir.z;
  to.time = from.time;
  to.mask = from.mask;
  to.Ng[0] = from.Ng.x;
  to.Ng[1] = from.Ng.y;
  to.Ng[2] = from.Ng.z;
  to.geomID = from.geomID;
  to.primID = from.primID;
  to.instID = from.instID;
  to.tnear = from.tnear;
  to.tfar = from.tfar;

  return true;
}


template<class B, class A>
inline bool conv_to_ray(const A & from, B & to)
{

  to.org.x = from.org[0];
  to.org.y = from.org[1];
  to.org.z = from.org[2];
  to.dir.x = from.dir[0];
  to.dir.y = from.dir[1];
  to.dir.z = from.dir[2];
  to.time = from.time;
  to.mask = from.mask;
  to.Ng.x = from.Ng[0];
  to.Ng.y = from.Ng[1];
  to.Ng.z = from.Ng[2];
  to.geomID = from.geomID;
  to.primID = from.primID;
  to.instID = from.instID;
  to.tnear = from.tnear;
  to.tfar = from.tfar;

  return true;
}






#endif  //  CONVERSIONS_HAS_BEEN_INCLUDED
#ifndef __Macros_h__
#define __Macros_h__

#define QUOTEDNAME(name) #name
#define EMBREE_TRY_STATEMENT(functionName) try {
#define EMBREE_CATCH_STATEMENT(functionName) } catch (std::exception e) { \
  FABRIC_EXT_SETERROR_AND_RETURN("%s: %s", QUOTEDNAME(functionName), e.what()); \
}
#define EMBREE_CATCH_STATEMENT(functionName) } catch (std::exception e) { \
  FABRIC_EXT_SETERROR_AND_RETURN("%s: %s", QUOTEDNAME(functionName), e.what()); \
}
#define EMBREE_CATCH_STATEMENT_RETURN(functionName, value) } catch (std::exception e) { \
  FABRIC_EXT_SETERROR_AND_RETURN_VALUE(value, "%s: %s", QUOTEDNAME(functionName), e.what()); \
}

#endif
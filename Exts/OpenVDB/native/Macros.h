#include <openvdb/Exceptions.h>
#ifndef __Macros_h__
#define __Macros_h__

#define QUOTEDNAME(name) #name
#define OPENVDB_TRY_STATEMENT(functionName) try {
#define OPENVDB_CATCH_STATEMENT(functionName) } catch (openvdb::Exception e) { \
  FABRIC_EXT_SETERROR_AND_RETURN("%s: %s", QUOTEDNAME(functionName), e.what()); \
}
#define OPENVDB_CATCH_STATEMENT(functionName) } catch (openvdb::Exception e) { \
  FABRIC_EXT_SETERROR_AND_RETURN("%s: %s", QUOTEDNAME(functionName), e.what()); \
}
#define OPENVDB_CATCH_STATEMENT_RETURN(functionName, value) } catch (openvdb::Exception e) { \
  FABRIC_EXT_SETERROR_AND_RETURN_VALUE(value, "%s: %s", QUOTEDNAME(functionName), e.what()); \
}

#endif
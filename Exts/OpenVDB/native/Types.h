#ifndef TYPES_HAS_BEEN_INCLUDED
#define TYPES_HAS_BEEN_INCLUDED


// #ifndef _WIN32
// #include <stdlib.h>
// #endif

#ifdef WIN32


#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>

#ifdef _DEBUG
#ifndef DEBUG_NEW
#define DEBUG_NEW   new( _NORMAL_BLOCK, __FILE__, __LINE__)
#endif
#else
#ifndef DEBUG_NEW
#define DEBUG_NEW
#endif
#endif

#else

#ifndef DEBUG_NEW
#define DEBUG_NEW
#endif

#endif          // WIN32


#include <iostream>
#include <openvdb/openvdb.h>
#include <boost/make_shared.hpp>
#include <boost/scoped_ptr.hpp>//camera in native ray tracer

#include <FabricEDK.h>

#include "include/Coord.h"
#include "include/CoordBBox.h"
#include "include/Vec3.h"
#include "include/Vec3_d.h"
#include "include/Transform.h"
#include "include/BBox_d.h"
#include "include/PerspectiveCamera.h"

#include <openvdb/tree/LeafManager.h>
#include <openvdb/tools/LevelSetSphere.h>
#include <openvdb/tools/LevelSetFracture.h>// MinMaxVoxel
#include <openvdb/tools/GridTransformer.h>// resampleToMatch
#include <openvdb/tools/Prune.h>// pruneInactive
#include <openvdb/tools/Morphology.h>// dilateVoxels
#include <openvdb/tools/RayIntersector.h>// VolumeRayIntersector
#include "openvdb/tools/MeshToVolume.h" //MeshToVolume

#include <openvdb/tools/RayTracer.h>// raytracer



#include "klAccessor.h"
#include "klGrid.h"
#include "klTree.h"

#include "Conversions.h"



using namespace Fabric::EDK;

template<typename T>
struct klFileVDBWrapper{
  T pointer;
  klFileVDBWrapper(const std::string& filename):pointer(filename){}
  klFileVDBWrapper(const T& other):pointer(other){}
};

template<typename T>
struct klFileVDB{
  typedef T Type;
  typedef klFileVDBWrapper<Type> Data;

  Type& data()
  {
    return (*mData).pointer;
  }

  const Type& data() const
  {
    return (*mData).pointer;
  }
  klFileVDB(const std::string& filename):mData( new Data(filename)){}
  klFileVDB(Type other):mData( new Data(other) ){};

  void clear(bool owned){
    if(owned)delete(mData);
    delete this;
  }


 private:
  Data * mData;
};

template<typename T, typename I>
struct StructPointerT2{
  typedef T Type;
  typedef I TypeI;
  typedef typename TypeI::Ptr Ptr;
  Type * pointer;
  Ptr pointerI;

  void clear(bool owned){
    if(owned)delete(pointer);
    delete this;
  }

};

template<typename T>
struct StructPointerT{
  typedef T Type;
  Type * pointer;

  void clear(bool owned){
    if(owned)delete(pointer);
    delete this;
  }

};

template<template <typename,typename> class Iter, typename GridT, typename IterT>
struct IteratorT{
  typedef Iter<GridT, IterT> Type;
  Type * iter;

  void clear(bool owned){
    if(owned)delete(iter);
    delete this;
  }

};

template<template <typename,typename> class Iter, typename GridT, typename IterT>
struct IteratorT<Iter, const GridT, IterT>{
  typedef Iter<const GridT, IterT> Type;
  Type * iter;

  void clear(bool owned){
    if(owned)delete(iter);
    delete this;
  }

};


// typedef openvdb::BoolGrid::ValueConverter<bool>::Type  BooleanGridVDB;
typedef openvdb::GridBase                   GridBaseVDB;
typedef klTypes::klBasePtr<GridBaseVDB>     GridBase;

typedef openvdb::math::Transform              Transform;
typedef klTypes::klBasePtr<openvdb::Metadata> MetaData;
typedef klFileVDB<openvdb::io::File>          FileVDB;

typedef openvdb::FloatGrid FloatGridVDB;
typedef openvdb::FloatTree FloatTreeVDB;

typedef klTypes::klGridTyped<FloatGridVDB>  FloatGrid;
typedef klTree::klTree<FloatTreeVDB> FloatTree;

typedef klAccessor::AccessorWrap<FloatGridVDB, FloatTreeVDB> FloatAccessor;
typedef klAccessor::AccessorWrap<const FloatGridVDB, const FloatTreeVDB> FloatConstAccessor;

typedef IteratorT<klTypes::IterWrap, FloatGridVDB, FloatGridVDB::ValueOnIter> FloatValueOnIter;

typedef IteratorT<klTypes::IterWrap, FloatTreeVDB, FloatTreeVDB::LeafNodeType::ValueOnIter> FloatLeafOnIter;
typedef IteratorT<klTypes::IterWrap, FloatTreeVDB, FloatTreeVDB::LeafNodeType::ValueAllIter> FloatLeafAllIter;

typedef IteratorT<klTypes::IterWrap, FloatTreeVDB, FloatTreeVDB::LeafIter> FloatLeafTreeIter;
typedef IteratorT<klTypes::IterWrap, const FloatTreeVDB, FloatTreeVDB::LeafCIter> FloatLeafTreeCIter;


typedef StructPointerT2<openvdb::tree::LeafManager<FloatTreeVDB>, FloatTreeVDB> FloatLeafManager;
typedef StructPointerT2<FloatTreeVDB::LeafNodeType, FloatTreeVDB> FloatLeafNode;
typedef StructPointerT2<const FloatTreeVDB::LeafNodeType, const FloatTreeVDB> FloatCLeafNode;

typedef openvdb::tools::GridSampler<FloatGridVDB::ConstAccessor, openvdb::tools::BoxSampler> GridSamplerFloat;
typedef openvdb::BoolGrid BooleanGridVDB;
typedef openvdb::BoolTree BooleanTreeVDB;

typedef klTypes::klGridTyped<BooleanGridVDB>  BooleanGrid;
typedef klTree::klTree<BooleanTreeVDB> BooleanTree;

typedef klAccessor::AccessorWrap<BooleanGridVDB, BooleanTreeVDB> BooleanAccessor;
typedef klAccessor::AccessorWrap<const BooleanGridVDB, const BooleanTreeVDB> BooleanConstAccessor;

typedef IteratorT<klTypes::IterWrap, BooleanGridVDB, BooleanGridVDB::ValueOnIter> BooleanValueOnIter;

typedef IteratorT<klTypes::IterWrap, BooleanTreeVDB, BooleanTreeVDB::LeafNodeType::ValueOnIter> BooleanLeafOnIter;
typedef IteratorT<klTypes::IterWrap, BooleanTreeVDB, BooleanTreeVDB::LeafNodeType::ValueAllIter> BooleanLeafAllIter;

typedef IteratorT<klTypes::IterWrap, BooleanTreeVDB, BooleanTreeVDB::LeafIter> BooleanLeafTreeIter;
typedef IteratorT<klTypes::IterWrap, const BooleanTreeVDB, BooleanTreeVDB::LeafCIter> BooleanLeafTreeCIter;


typedef StructPointerT2<openvdb::tree::LeafManager<BooleanTreeVDB>, BooleanTreeVDB> BooleanLeafManager;
typedef StructPointerT2<BooleanTreeVDB::LeafNodeType, BooleanTreeVDB> BooleanLeafNode;
typedef StructPointerT2<const BooleanTreeVDB::LeafNodeType, const BooleanTreeVDB> BooleanCLeafNode;

typedef openvdb::tools::GridSampler<BooleanGridVDB::ConstAccessor, openvdb::tools::BoxSampler> GridSamplerBoolean;

typedef StructPointerT< openvdb::tools::VolumeRayIntersector<FloatGridVDB> > VolumeRayIntersector;
typedef StructPointerT< boost::scoped_ptr<openvdb::tools::BaseCamera> > BaseCamera;

#endif  //  TYPES_HAS_BEEN_INCLUDED

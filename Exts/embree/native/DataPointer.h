#ifndef __DATA_POINTER_H__
#define __DATA_POINTER_H__


template <class Type>
struct DataPointer
{
public:
  DataPointer(Type * data, bool owned = false)
  {
    Fabric::EDK::AtomicUInt32SetValue( 1, &m_refCount );
    m_data = data;
    m_owned = owned;
  }

  void retain()
  {
    Fabric::EDK::AtomicUInt32Increment( &m_refCount );
  }

  void clear()
  {
    if(m_data != NULL && m_owned)m_data->clear(m_owned);
    delete this;
  }

  void release()
  {
    if ( Fabric::EDK::AtomicUInt32DecrementAndGetValue( &m_refCount ) == 0 )clear();
  }

  Type *data()
  {
    return m_data;
  }

  const Type * data() const
  {
    return m_data;
  }

  const uint32_t & count() const
  {
    return m_refCount;
  }
private:
  Type * m_data;
  bool m_owned;
  uint32_t m_refCount;
};

#endif

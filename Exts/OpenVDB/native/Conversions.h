#ifndef CONVERSIONS_HAS_BEEN_INCLUDED
#define CONVERSIONS_HAS_BEEN_INCLUDED


#include <openvdb/openvdb.h>
#include <openvdb/Types.h>

#include <FabricEDK.h>

#include "include/Coord.h"
#include "include/CoordBBox.h"
#include "include/Vec3.h"
#include "include/Vec3_d.h"
#include "include/Mat44.h"
#include "include/Transform_impl.h"
#include "include/BBox_d.h"
#include "include/TimeSpan.h"
#include "include/VdbRay.h"
#include "include/PerspectiveCamera.h"

#include "DataPointer.h"

using namespace Fabric::EDK;

#ifdef WIN32
#ifdef _DEBUG
#define new DEBUG_NEW
#endif
#endif


typedef openvdb::math::Ray<double> Ray;
typedef Ray::TimeSpan vdbTimeSpan;
typedef openvdb::math::Transform TransformVDB;


template<class A, class B>
inline bool conv_from_dummy(const A & a, B & b)
{
  b = B();
  return true;
}

template<class A, class B>
inline bool conv_from_baseType(const A & a, B & b)
{
  b = a;
  return true;
}

template<class B, class A>
inline bool conv_to_baseType(const A & a, B & b)
{
  b = a;
  return true;
}


inline bool conv_from_String(const KL::String & a, std::string & b)
{
  if(a.data() == NULL)
    b = "";
  else
    b = a.data();
  return true;
}

inline bool conv_to_String(const std::string & a, KL::String & b)
{
  if(a.length() == 0)
    b = "";
  else
    b = a.c_str();
  return true;
}

inline bool conv_from_Vec3f(const KL::Vec3 & a, openvdb::Vec3f & b)
{
  b=openvdb::Vec3f(a.x,a.y,a.z);
  return true;
}

inline bool conv_to_Vec3f(const openvdb::Vec3f & a, KL::Vec3 & b)
{
  b.x = a[0];
  b.y = a[1];
  b.z = a[2];
  return true;
}

inline bool conv_from_Vec3d(const KL::Vec3_d & a, openvdb::Vec3d & b)
{
  b=openvdb::Vec3d(a.x,a.y,a.z);
  return true;
}

inline bool conv_to_Vec3d(const openvdb::Vec3d & a, KL::Vec3_d & b)
{
  b.x = a[0];
  b.y = a[1];
  b.z = a[2];
  return true;
}

inline bool CoordFromKL(const KL::Coord & from, openvdb::Coord & to) {
  to = openvdb::Coord(from.i,from.j,from.k);
  return true;
}

inline bool CoordToKL(const openvdb::Coord & from, KL::Coord & to) {
  to.i=from[0];
  to.j=from[1];
  to.k=from[2];
  return true;
}


inline bool BBoxdFromKL(const KL::BBox_d & from, openvdb::math::BBox<openvdb::Vec3d> & to) {
  openvdb::Vec3d min(from.min.x,from.min.y,from.min.z);
  openvdb::Vec3d max(from.max.x,from.max.y,from.max.z);

  to = openvdb::math::BBox<openvdb::Vec3d>(min,max);
  return true;
}

inline bool BBoxdToKL(const openvdb::math::BBox<openvdb::Vec3d> & from, KL::BBox_d & to) {
  to.min.x=from.min()[0];
  to.min.y=from.min()[1];
  to.min.z=from.min()[2];
  to.max.x=from.max()[0];
  to.max.y=from.max()[1];
  to.max.z=from.max()[2];
  return true;
}


inline bool CoordBBoxFromKL(const KL::CoordBBox & from, openvdb::CoordBBox & to) {
  openvdb::Coord min(from.min.i,from.min.j,from.min.k);
  openvdb::Coord max(from.max.i,from.max.j,from.max.k);

  to = openvdb::CoordBBox(min,max);
  return true;
}

inline bool CoordBBoxToKL(const openvdb::CoordBBox & from, KL::CoordBBox & to) {
  to.min.i=from.min()[0];
  to.min.j=from.min()[1];
  to.min.k=from.min()[2];
  to.max.i=from.max()[0];
  to.max.j=from.max()[1];
  to.max.k=from.max()[2];
  return true;
}

inline bool TimeSpanFromKL(const KL::TimeSpan & from, vdbTimeSpan& to) {
  to.set(from.t0, from.t1);

  return true;
}

inline bool TimeSpanToKL(const vdbTimeSpan & from, KL::TimeSpan & to) {
  to.t0 = from.t0;
  to.t1 = from.t1;
  return true;
}

inline bool VdbRayFromKL(const KL::VdbRay & from, Ray& to) {
  to.setEye(openvdb::Vec3d(from.mEye.x, from.mEye.y, from.mEye.z));
  to.setDir(openvdb::Vec3d(from.mDir.x, from.mDir.y, from.mDir.z));
  to.setTimes(from.mTimeSpan.t0, from.mTimeSpan.t1);
  return true;
}

inline bool VdbRayToKL(const Ray & from, KL::VdbRay & to) {
  conv_to_Vec3d(from.eye(), to.mEye);
  conv_to_Vec3d(from.dir(), to.mDir);
  conv_to_Vec3d(from.invDir(), to.mInvDir);
  to.mTimeSpan.t0 = from.t0();
  to.mTimeSpan.t1 = from.t1();
  return true;
}



// Transform
//
inline bool conv_from_transform(const KL::Transform & a, TransformVDB * & b)
{
  if(!a.isValid())
    return false;
  TransformVDB * px = (TransformVDB *)a->pointer;
  if(px == NULL)
    return false;
  b = px;
  return true;
}

inline bool conv_to_transform(const TransformVDB & a, KL::Transform & b)
{

  if(!b.isValid())
    b = KL::Transform::Create();
  b->pointer = new TransformVDB(a);
  return true;
}


inline bool Mat4RFromKL(const KL::Mat44 & from, openvdb::Mat4R & to) {
  KL::Vec4 row0 = from.row0;
  KL::Vec4 row1 = from.row1;
  KL::Vec4 row2 = from.row2;
  KL::Vec4 row3 = from.row3;
  to = openvdb::Mat4R(row0.x, row0.y, row0.z, row0.t,
                      row1.x, row1.y, row1.z, row1.t,
                      row2.x, row2.y, row2.z, row2.t,
                      row3.x, row3.y, row3.z, row3.t);
  return true;
}

inline bool Mat4RToKL(const openvdb::Mat4R & from, KL::Mat44 & to) {
  to.row0.x = *from[0];
  to.row0.y = *from[1];
  to.row0.z = *from[2];
  to.row0.t = *from[3];

  to.row1.x = *from[4];
  to.row1.y = *from[5];
  to.row1.z = *from[6];
  to.row1.t = *from[7];

  to.row2.x = *from[8];
  to.row2.y = *from[9];
  to.row2.z = *from[10];
  to.row2.t = *from[11];

  to.row3.x = *from[12];
  to.row3.y = *from[13];
  to.row3.z = *from[14];
  to.row3.t = *from[15];

  return true;
}



template<typename B, typename A>
inline bool conv_to_struct(const A * a, B & b, bool owned = false)
{
  Fabric::EDK::KL::DataWrapper< DataPointer<A> > out( b.pointer );
  if( ((DataPointer<A>*)b.pointer) != NULL )out->release();
  out = new DataPointer<A>((A*)a, owned);
  return true;
}


template<class A, class B>
inline bool conv_from_struct(const A & a, B * & b)
{
  Fabric::EDK::KL::ConstDataWrapper<  DataPointer<B>  > in( a.pointer );
  if (!in)
    return false;
  b = (B *)in->data();
  return true;
}


template<class A, class B>
inline bool conv_from_object(const A & a, B * & b)
{
  if(!a.isValid())
    return false;
  B * in = (B *)a->pointer;
  if(in == NULL)
    return false;
  b = in;
  return true;
}


template<class B, class A>
inline bool conv_to_object(const A * a, B & b, bool owned = false)
{
  if(!b.isValid())
    b = B::Create();
  if(b->pointer != NULL)
    ((A *)b->pointer)->release();
  b->pointer = new A( a->data(), owned);
  return true;
}


template<typename A, typename B>
inline bool conv_from_struct_ptr(const A & a, typename B::Ptr & b)
{
  typedef typename B::Ptr Ptr;
  Fabric::EDK::KL::ConstDataWrapper<  B  > in( a.pointer );
  if (!in)
    return false;
  b = Ptr(in->data());
  return true;
}


template<typename B, typename A>
inline bool conv_to_struct_ptr(const typename A::Ptr & a, B & b, bool owned = false)
{
  if(((A *)b.pointer) != NULL)
    ((A *)b.pointer)->release();
  b.pointer = new A(a, owned);
  return true;
}

template<typename B, typename A>
inline bool conv_to_object1(const typename A::Ptr & a, B & b, bool owned = false)
{

  if(!b.isValid())
    b = B::Create();
  if(b->pointer != NULL)
    ((A *)b->pointer)->release();

  b->pointer = new A(a,true);

  return true;

}

template<typename PointType>
struct TriangleMeshAdapter {

  TriangleMeshAdapter(PointType pointArray)
      : mPointArray(pointArray)
      , mPointArraySize(pointArray.size())
      , mPolygonArraySize(pointArray.size()/3.0f)
  {
  }
  size_t polygonCount() const { return mPolygonArraySize; }
  size_t pointCount() const { return mPointArraySize; }

  /// @brief  Vertex count for polygon @a n
  size_t vertexCount(size_t n) const {
      return 3;
  }

  /// @brief  Returns position @a pos in local grid index space
  ///         for polygon @a n and vertex @a v
  void getIndexSpacePoint(size_t n, size_t v, openvdb::Vec3d& pos) const {
      const KL::Vec3 p = mPointArray[(n * 3 ) + int(v)];
      pos[0] = double(p.x);
      pos[1] = double(p.y);
      pos[2] = double(p.z);
  }
private:
  PointType      mPointArray;
  size_t         mPointArraySize;
  size_t         mPolygonArraySize;
};



#endif  //  CONVERSIONS_HAS_BEEN_INCLUDED
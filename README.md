## kl-externals

This repository contains wrapping of the C/C++ library of `OpenVDB`, and `embree`.


## OpenVDB
http://www.openvdb.org/
> The OpenVDB library comprises a hierarchical data structure and a suite of tools for the efficient manipulation of sparse, possibly time-varying, volumetric data discretized on a three-dimensional grid. It is based on VDB, which was developed by Ken Museth at DreamWorks Animation.


##### Facts:
 - Wraps a good portion of the library covering grid, tree, most of the iterators, accessors, with KL types for both non-const and const eg: `FloatAccessor`, `FloatConstAccessor`...
 - Also wraps some of the tools for volumetric rendering and for levelsets
 - Wraps only `flaot32` and `bool` fields.
 - KL sources are partially generated through a template mechanism. This is done to create the various typed of fields, eg: `struct <Template>LeafOnIter{` will become `struct FloatGridLeafOnIter{`

##### Depends on:
 - A static build of OpenVDB (http://www.openvdb.org/download/), version 3.1.1 or higher.
 - A static build of TBB (https://www.threadingbuildingblocks.org/download), version 4.1 or higher.
 - A static build of boost (http://www.boost.org/), version 1.51 or higher.
 - A static build of Half (http://www.openexr.com/downloads.html), version 2.0.0 or higher.
 - A static build of zlib (http://www.zlib.net/), version 1.2.8 or higher.

##### Builds with:
 - Windows: vs2014
 - Linux: gcc-4.6.3




## embree
https://embree.github.io/
> Embree is a collection of high-performance ray tracing kernels, developed at Intel....
> The kernels are optimized for photo-realistic rendering on the latest Intel® processors with support for SSE, AVX, AVX2, and the 16-wide Intel® Xeon Phi™ coprocessor vector instructions.

##### Facts:
- embree wraps the user `rtcore_geometry` (user geometry described as a bounding box from a kl callback)
- `rtcScene`, and `rtcore_geometry` ancillary functions

##### Depends on:
 - A static build of embree (https://embree.github.io/downloads.html), version 2.4 or higher.
 - A static build of TBB (https://www.threadingbuildingblocks.org/download), version 4.1 or higher.

##### Builds with:
 - Windows:
		2.8.0: Visual Studio Community 2015
		2.4.0: Visual Studio 2013
		2.3.3: Tested with Visual Studio 2010 and below
 - Linux: gcc-4.6.3

## Building
A scons (http://www.scons.org/) build script has been tested on windows, and linux.

`scons help`
Lists all the variables used to configure OpenVDB, and embree. The variables can be defined in a python file named external.py next to SConstruct.
There is a external.py.template which can be copied, and populated accordingly.

example output:                                   
```
...
The architecture you are building for (x86|x86_64)
FABRIC_BUILD_ARCH
Default: x64_64
Current: x86_64
...
...
FABRIC_DIR
Fabric root ( /path/to/FABRIC_DIR )
Default: PATH/TO/FABRIC
Current: {default}
...
```

`scons .`
`scons OpenVDB`
builds the OpenVDB library, and installs its kl extensions

`scons embree`
builds and installs the embree library its kl extensions

`scons clean`
removes OpenVDB and embree kl2edk folders, and theirs intermediate directories

## KL sources
`python Exts/OpenVDB/templatekl.py Exts/OpenVDB/*.tjson Exts/OpenVDB/native/Types.th Exts/OpenVDB/kl/*.tkl`
re-generate all the kl sources with the templates expanded - run from the root of repository.

For example:
```
<Template> = Boolean::JSON,(KL::Boolean)
<Template> = Float::JSON,(KL::Float32)

#startloop
"<Template>Accessor": {
  "ctype": "<Template>Accessor *",
...
  return <Template::JSON> localThis_->iter->iter().getValue();
},
#endloop
```

will be generated as:

```
"BooleanAccessor": {
  "ctype": "BooleanAccessor *",
...
  return (KL::Boolean) localThis_->iter->iter().getValue();
},
"FloatAccessor": {
  "ctype": "FloatAccessor *",
...
  return (KL::Float32) localThis_->iter->iter().getValue();
},
```
###### build notes:
the generated extensio.cpp looks like this:
```
IMPLEMENT_FABRIC_EDK_ENTRIES_WITH_SETUP_CALLBACK( OpenVDB, Openvdb_initialize )
```
instead of:

```
IMPLEMENT_FABRIC_EDK_ENTRIES( OpenVDB )
```

`Openvdb_initialize` is a global function autogenerated as:

```
FABRIC_EXT_EXPORT void Openvdb_initialize(
```

In doing so the OpenVDB library is initialized as soon as the library is loaded, however it's not unloaded resulting in a minor leak :)



## dfg
dfg presets need to be created manually after running scons :unamused:
source the fabric environment, then run something like this:

```bash
!#/bin/bash
export FABRIC_EXTS_PATH=$FABRIC_EXTS_PATH:`pwd`/install/fe-2.0/Exts
export FABRIC_DFG_PATH=$FABRIC_DFG_PATH:`pwd`/install/fe-2.0/dfg
rm -rf install/fe-2.0/dfg/OpenVDB
mkdir -p install/fe-2.0/dfg/OpenVDB
kl2dfg -polymorphism install/fe-2.0/Exts/OpenVDB/OpenVDB.fpm.json install/fe-2.0/dfg/OpenVDB
```

## Platforms
Tested on Windows, and Linux.


Copyright
---------
This software is released under the MIT licence

Copyright (c) 2016 Fabio Piparo

Permission is hereby granted, free of charge, to any person obtaining a copy of 
this software and associated documentation files (the "Software"), to deal in 
the Software without restriction, including without limitation the rights to 
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of 
the Software, and to permit persons to whom the Software is furnished to do so, 
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS 
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR 
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER 
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

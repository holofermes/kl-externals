import os
import sys
import argparse
from pprint import pprint as pp
from collections import defaultdict

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('kl', metavar='/path/to/file.kl', type=str, nargs='+',
                   help='path to kl files')


args = parser.parse_args()

T = '<Template>'
T_T = '<Template::'
T_EQ = T+"="
NS_SEP = '::'
START = 1
START_END = 2
LOOP = 3

def findLoops(start, lines):
  loops = []
  stop = False
  end = len(lines)
  _start = start
  ll = _start
  while not stop:
    for ll in range(_start, end):
      if lines[ll].startswith('#startloop'):
        break
    lll = ll
    for lll in range(ll, end):
      if lines[lll].startswith('#endloop'):
        rangeloop = (ll, lll)
        loops.append(rangeloop)
        _start = lll
        if lll+1 == end:
          stop = True
        break
    if lll+1 == end:
      stop = True
  return  loops


allowedFormats=[".tkl", ".tjson", ".th"]
for klfile in args.kl:
  definitions = defaultdict(list)
  otherDefLUT = {}
  print "reading file: " + klfile
  outPath, fileNameExt = os.path.split(klfile)
  fileName, text = os.path.splitext(fileNameExt)
  if text not in allowedFormats:
    sys.stderr.write("format not allowed for file: " + klfile + "\n")
    sys.exit(1)
  outName = fileName.replace("Template", "")
  extUP = text[2:].upper()
  # build template definition from file
  # 
  with open(klfile) as f:
    # build definitions
    # 
    lines = f.readlines()
    nLines = len(lines)
    l=0
    for l in range(nLines):
      nospace = "".join(lines[l].split())
      ldef =  nospace.find(T_EQ)
      if ldef != -1:
        namespace = nospace[len(T_EQ):].split(NS_SEP, 1)

        value = namespace.pop(0)

        if len(namespace) :
          for ns in namespace:
            ext, otherValue = ns.split(',')
            extKey = ext + NS_SEP + value

            otherDefLUT[extKey] = otherValue
        definitions[T].append(value)
      else:
        break
  if fileName.find("Template") != -1:
    outName = definitions[T] + outName


  count = 0
  outKLfile = os.path.join(outPath, outName + "." + text[2:])
  # if there are no loops just replace line by line
  # 
  if len(definitions.values()[0]) == 1:
    print "Writing file: " + outKLfile

    with open(outKLfile, 'w') as of:
      for t, values in definitions.iteritems():
        print "applying value: %s for: %s " % (", ".join(values), t)
        for value in values:
          tKey = T_T + extUP + t[-1:]
          extKey = extUP + NS_SEP + value
          otherValue = otherDefLUT.get(extKey, False)
          for ll in range(l, len(lines)):
            line = lines[ll]
            if not otherValue:
              tStart = line.find(tKey)
              if tStart != -1:
                print "found template value but no language definitions are available. using value"
                line = line.replace(tKey, value)
            tStart = line.find(t)
            if tStart != -1:
              count+=1
              line = line.replace(t, value)
            if otherValue:
              tStart = line.find(tKey)
              if tStart != -1:
                count+=1
                line = line.replace(tKey, otherValue)

            of.write(line)

  # find loops 
  # 
  loops = findLoops(l, lines)
  nLines = len(lines)

  chunks = []
  print "generating chunks"
  firstChunk = nLines if not loops else loops[0][0]
  firstChunk = (l, firstChunk)
  chunks.append(firstChunk)

  for rangeloop in loops:
    for nLoop, value in enumerate(definitions.values()[0]):
      loopLength = 0
      start = rangeloop[0] + loopLength * nLoop
      end = rangeloop[1] + loopLength * nLoop
      chunks.append((start+1, end, value))

    chunks.append((end+1,))
  
  # re-arrange chunks:
  # 
  nChunks = len(chunks)

  for c in range(nChunks):
    if len(chunks[c]) == START:
      if c == nChunks-1:
        chunks[c] = (chunks[c][0], nLines)
        continue
      toChunk = chunks[c+1]
      toLine = toChunk[0]
      if len(toChunk) == LOOP:
        toLine -= 1
      chunks[c] = (chunks[c][0], toLine)

  def resolve(chunk, lines, default):
    resolved = []
    state = len(chunk)
    if state == START_END:
      value =  default
    elif state == LOOP:
      value =  chunk[2]
    tKey = T_T + extUP + T[-1:]
    extKey = extUP + NS_SEP + value
    otherValue = otherDefLUT.get(extKey, False)
    for line in lines[chunk[0]:chunk[1]]:
      if not otherValue:
        tStart = line.find(tKey)
        if tStart != -1:
          print "found template value but no language definitions are available. using value"
          line = line.replace(tKey, value)
      tStart = line.find(T)
      if tStart != -1:
        line = line.replace(T, value)
      if otherValue:
        tStart = line.find(tKey)
        if tStart != -1:
          line = line.replace(tKey, otherValue)
      resolved.append(line)
    return resolved

  print "Writing file: " + outKLfile
  with open(outKLfile, 'w') as of:
    _lines = []
    for chunk in chunks:
      _lines += resolve(chunk, lines, default=definitions.values()[0][0])
    for line in _lines:
      of.write(line)

  print "replaced: " + str(count) + " keywords." 

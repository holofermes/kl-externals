/*! Axis aligned bounding box representation */
struct RTCBounds{
  Float32 lower_x, lower_y, lower_z;
  Float32 upper_x, upper_y, upper_z;
};

interface RTCBoundsI{
  boundsFunc?(Size item, io RTCBounds bounds_o);
  intersectFunc?(Size item, io RTCRay ray_o);
  occludedFunc?(Size item, io RTCRay ray_o);
  Size itemCount?();
};


object RTCBoundsFunc: RTCBoundsI{};

function RTCBoundsFunc.boundsFunc?(Size item, io RTCBounds bounds_o){
  Ref<RTCBoundsI> bounds = this;
  bounds.boundsFunc(item, bounds_o);

}

function RTCBoundsFunc.intersectFunc?(/*io Ref<Object> ptr, */Size item, io RTCRay ray_o){
  Ref<RTCBoundsI> bounds = this;
  bounds.intersectFunc(/*ptr,*/ item, ray_o);
}

function RTCBoundsFunc.occludedFunc?(/*io Ref<Object> ptr, */Size item, io RTCRay ray_o){
  Ref<RTCBoundsI> bounds = this;
  bounds.occludedFunc(/*ptr,*/ item, ray_o);
}

function Size RTCBoundsFunc.itemCount?(){
  Ref<RTCBoundsI> bounds = this;
  return bounds.itemCount();
}


// function RTCBoundsFunc.boundsFuncSetup?(RTCScene scene, UInt32 geomID) = "boundsFuncSetup";

/*! Creates a new user geometry object. This feature makes it possible
 *  to add arbitrary types of geometry to the scene by providing
 *  appropiate bounding, intersect and occluded functions. A user
 *  geometry object is a set of user geometries. As the rtcIntersect
 *  and rtcOccluded functions support different ray packet sizes, the
 *  user also has to provide different versions of intersect and
 *  occluded function pointers for these packet sizes. However, the
 *  ray packet size of the called function pointer always matches the
 *  packet size of the originally invoked rtcIntersect and rtcOccluded
 *  functions. A user data pointer, that points to a user specified
 *  representation of the geometry, is passed to each intersect and
 *  occluded function invokation, as well as the index of the geometry
 *  of the set to intersect. */
function UInt32 rtcNewUserGeometry (RTCScene scene,        /*!< the scene the user geometry set is created in */
                                    Size numGeometries   /*!< the number of geometries contained in the set */) = "embree_rtcNewUserGeometry";

/*! Set data pointer for intersect and occluded functions. Invokations
 *  of the various user intersect and occluded functions get passed
 *  this data pointer when called. */

function rtcSetUserData(RTCScene scene, UInt32 geomID,  RTCBoundsFunc ptr) = "embree_rtcSetUserData";
// function rtcSetUserData(RTCScene scene, UInt32 geomID, io Ref<Object> ptr) = "embree_rtcSetUserData";

/*! Sets the bounding function to calculate bounding boxes of the user
 *  geometry items when building spatial index structures. The
 *  calculated bounding box have to be conservative and should be
 *  tight.*/
function rtcSetBoundsFunction(RTCScene scene, UInt32 geomID) = "embree_rtcSetBoundsFunction";
// function rtcSetBoundsFunction (RTCScene scene, UInt32 geomID, io Ref<RTCBoundsFunc> bounds) = "embree_rtcSetBoundsFunction";

/*! Set intersect function for single rays. The rtcIntersect function
 *  will call the passed function for intersecting the user
 *  geometry. */
function rtcSetIntersectFunction(RTCScene scene, UInt32 geomID/*, Ref<RTCIntersectFunc> intersect*/) = "embree_rtcSetIntersectFunction";
function rtcSetOccludedFunction(RTCScene scene, UInt32 geomID/*, Ref<RTCIntersectFunc> intersect*/) = "embree_rtcSetOccludedFunction";
// function rtcSetIntersectFunction (RTCScene scene, UInt32 geomID, Ref<RTCIntersectFunc> intersect) = "embree_rtcSetIntersectFunction";

function rtcUpdate(RTCScene scene, UInt32 geomID) = "embree_rtcUpdate";
// /*! Set intersect function for ray packets of size 4. The
//  *  rtcIntersect4 function will call the passed function for
//  *  intersecting the user geometry. */
// function rtcSetIntersectFunction4 (RTCScene scene, UInt32 geomID, RTCIntersectFunc4 intersect4);

// /*! Set intersect function for ray packets of size 8. The
//  *  rtcIntersect8 function will call the passed function for
//  *  intersecting the user geometry.*/
// function rtcSetIntersectFunction8 (RTCScene scene, UInt32 geomID, RTCIntersectFunc8 intersect8);

// /*! Set intersect function for ray packets of size 16. The
//  *  rtcIntersect16 function will call the passed function for
//  *  intersecting the user geometry. */
// function rtcSetIntersectFunction16 (RTCScene scene, UInt32 geomID, RTCIntersectFunc16 intersect16);

// /*! Set occlusion function for single rays. The rtcOccluded function
//  *  will call the passed function for intersecting the user
//  *  geometry. */
// function rtcSetOccludedFunction (RTCScene scene, UInt32 geomID, Ref<RTCOccludedFunc> occluded) = "embree_rtcSetOccludedFunction";

// /*! Set occlusion function for ray packets of size 4. The rtcOccluded4
//  *  function will call the passed function for intersecting the user
//  *  geometry. */
// function rtcSetOccludedFunction4 (RTCScene scene, UInt32 geomID, RTCOccludedFunc4 occluded4);

// ! Set occlusion function for ray packets of size 8. The rtcOccluded8
//  *  function will call the passed function for intersecting the user
//  *  geometry.
// function rtcSetOccludedFunction8 (RTCScene scene, UInt32 geomID, RTCOccludedFunc8 occluded8);

// /*! Set occlusion function for ray packets of size 16. The
//  *  rtcOccluded16 function will call the passed function for
//  *  intersecting the user geometry. */
// function rtcSetOccludedFunction16 (RTCScene scene, UInt32 geomID, RTCOccludedFunc16 occluded16);




/*! \brief Enable geometry. Enabled geometry can be hit by a ray. */
function rtcEnable(RTCScene scene, UInt32 geomID) = "embree_rtcEnable";
/*! \brief Disable geometry. 

  Disabled geometry is not hit by any ray. Disabling and enabling
  geometry gives higher performance than deleting and recreating
  geometry. */
function rtcDisable(RTCScene scene, UInt32 geomID) = "embree_rtcDisable";












// /*! Type of bounding function. */
// function embree_RTCBoundsFunc(io Ref<Object> ptr,              /*!< pointer to user data */
//                               Size item,            /*!< item to calculate bounds for */
//                               RTCBounds bounds_o     /*!< returns calculated bounds */) = "embree_embree_RTCBoundsFunc";

// /*! Type of intersect function pointer for single rays. */
// function embree_RTCIntersectFunc(io Ref<Object> ptr,           /*!< pointer to user data */
//                                  RTCRay ray,         /*!< ray to intersect */
//                                  Size item          /*!< item to intersect */) = "embree_embree_RTCIntersectFunc";

// // /*! Type of intersect function pointer for ray packets of size 4. */
// // function embree_RTCIntersectFunc4(Data valid,  /*!< pointer to valid mask */
// //                                   io Ref<Object> ptr,          /*!< pointer to user data */
// //                                   RTCRay4 ray,       /*!< ray packet to intersect */
// //                                   Size item         /*!< item to intersect */);

// // /*! Type of intersect function pointer for ray packets of size 8. */
// // function embree_RTCIntersectFunc8(Data valid,  /*!< pointer to valid mask */
// //                                   io Ref<Object> ptr,          /*!< pointer to user data */
// //                                   RTCRay8 ray,       /*!< ray packet to intersect */
// //                                   Size item         /*!< item to intersect */);

// // /*! Type of intersect function pointer for ray packets of size 16. */
// // function embree_RTCIntersectFunc16(Data valid, /*!< pointer to valid mask */
// //                                    io Ref<Object> ptr,         /*!< pointer to user data */
// //                                    RTCRay16 ray,     /*!< ray packet to intersect */
// //                                    Size item        /*!< item to intersect */);

// ! Type of occlusion function pointer for single rays.
// function embree_RTCOccludedFunc( io Ref<Object> ptr,           /*!< pointer to user data */
//                                  RTCRay ray,         /*!< ray to test occlusion */
//                                  Size item          /*!< item to test for occlusion */) = "embree_embree_RTCOccludedFunc";

// // /*! Type of occlusion function pointer for ray packets of size 4. */
// // function embree_RTCOccludedFunc4( Data valid,  /*! pointer to valid mask */
// //                                   io Ref<Object> ptr,          /*!< pointer to user data */
// //                                   RTCRay4 ray,       /*!< Ray packet to test occlusion. */
// //                                   Size item         /*!< item to test for occlusion */);

// // /*! Type of occlusion function pointer for ray packets of size 8. */
// // function embree_RTCOccludedFunc8( Data valid,  /*! pointer to valid mask */
// //                                   io Ref<Object> ptr,          !< pointer to user data
// //                                   RTCRay8 ray,       /*!< Ray packet to test occlusion. */
// //                                   Size item         /*!< item to test for occlusion */);

// // /*! Type of occlusion function pointer for ray packets of size 16. */
// // function embree_RTCOccludedFunc16( Data valid, /*! pointer to valid mask */
// //                                    io Ref<Object> ptr,         /*!< pointer to user data */
// //                                    RTCRay16 ray,     /*!< Ray packet to test occlusion. */
// //                                    Size item        /*!< item to test for occlusion */);

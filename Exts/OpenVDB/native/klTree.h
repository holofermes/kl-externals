#ifndef KL_TREE_HAS_BEEN_INCLUDED
#define KL_TREE_HAS_BEEN_INCLUDED

#include <FabricEDK.h>

using namespace Fabric::EDK;

namespace klTree{


using namespace Fabric::EDK;

template<typename TreeType>
class klTree
{
public:
  typedef TreeType Type;
  typedef typename TreeType::Ptr Ptr;
  typedef typename TreeType::ValueType ValueType;

  klTree(bool owned = false) : mTree()
  {
    Fabric::EDK::AtomicUInt32SetValue( 1, &mRefCount );
    mOwned = owned;
  }

  klTree(Ptr otherTree,bool owned = false) : mTree( otherTree )
  {
    Fabric::EDK::AtomicUInt32SetValue( 1, &mRefCount );
    mOwned = owned;
  }

  void retain()
  {
    Fabric::EDK::AtomicUInt32Increment( &mRefCount );
  }

  void release()
  {
    if ( AtomicUInt32DecrementAndGetValue( &mRefCount ) == 0 )delete this;
  }


  void setData(Ptr& data)
  {
    mTree = data;
  }

  Ptr& data()
  {
    return mTree;
  }

  const Ptr& data() const
  {
    return mTree;
  }

private:  
  Ptr mTree;
  bool mOwned;
  uint32_t mRefCount;
};


}//ending klTree namespace

#endif // KL_TREE_HAS_BEEN_INCLUDED
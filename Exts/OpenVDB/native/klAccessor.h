#ifndef KL_ACCESSOR_HAS_BEEN_INCLUDED
#define KL_ACCESSOR_HAS_BEEN_INCLUDED
#include <openvdb/math/Operators.h>//ISGradient

namespace klAccessor {


using namespace openvdb::OPENVDB_VERSION_NAME;


/// Type traits for grid accessors
template<typename GridOrTreeType, typename _TreeT>
struct AccessorTraits
{


  typedef typename TreeAdapter<GridOrTreeType>::GridType                GridType;
  typedef typename TreeAdapter<GridOrTreeType>::TreeType                TreeType;
  typedef typename TreeAdapter<GridOrTreeType>::NonConstGridPtrType     GridPtrT;
  typedef typename TreeAdapter<GridOrTreeType>::NonConstTreePtrType     TreePtrT;
  typedef typename TreeAdapter<GridOrTreeType>::NonConstAccessorType    AccessorT;
  typedef typename AccessorT::ValueType                       ValueT;

  // typedef _TreeT                              TreeT;
  // typedef TreeT                               NonConstTreeT;
  // typedef typename NonConstTreeT::Ptr         TreePtrT;

  static const bool IsConst = false;

  static const char* typeName() { return "Accessor"; }

  static void setActiveState(AccessorT& acc, const Coord& ijk, bool on) {
    acc.setActiveState(ijk, on);
  }
  static void setValueOnly(AccessorT& acc, const Coord& ijk, const ValueT& val) {
    acc.setValueOnly(ijk, val);
  }
  static void setValueOn(AccessorT& acc, const Coord& ijk) { acc.setValueOn(ijk); }
  static void setValueOn(AccessorT& acc, const Coord& ijk, const ValueT& val) {
    acc.setValueOn(ijk, val);
  }
  static void setValueOff(AccessorT& acc, const Coord& ijk) { acc.setValueOff(ijk); }
  static void setValueOff(AccessorT& acc, const Coord& ijk, const ValueT& val) {
    acc.setValueOff(ijk, val);
  }
};

// Partial specialization for const accessors
template<typename GridOrTreeType, typename _TreeT>
struct AccessorTraits<const GridOrTreeType, const _TreeT>
{

  typedef typename TreeAdapter<GridOrTreeType>::GridType             GridType;
  typedef typename TreeAdapter<GridOrTreeType>::TreeType             TreeType;
  typedef typename TreeAdapter<GridOrTreeType>::ConstGridPtrType     GridPtrT;
  typedef typename TreeAdapter<GridOrTreeType>::ConstTreePtrType     TreePtrT;
  typedef typename TreeAdapter<GridOrTreeType>::ConstAccessorType    AccessorT;
  typedef typename AccessorT::ValueType                              ValueT;

  // typedef const _TreeT                            TreeT;
  // typedef _TreeT                                  NonConstTreeT;
  // typedef typename NonConstTreeT::ConstPtr        TreePtrT;


  static const bool IsConst = true;

  static const char* typeName() { return "ConstAccessor"; }

  static void setActiveState(AccessorT&, const Coord&, bool) { notWritable(); }
  static void setValueOnly(AccessorT&, const Coord&, const ValueT&) { notWritable(); }
  static void setValueOn(AccessorT&, const Coord&) { notWritable(); }
  static void setValueOn(AccessorT&, const Coord&, const ValueT&) { notWritable(); }
  static void setValueOff(AccessorT&, const Coord&) { notWritable(); }
  static void setValueOff(AccessorT&, const Coord&, const ValueT&) { notWritable(); }

  static void notWritable()
  {
    printf("accessor is read-only");
  }
};

template<typename GridOrTreeType, typename _TreeType>
class AccessorWrap
{
public:
  typedef GridOrTreeType Type;
  typedef AccessorTraits<GridOrTreeType, _TreeType>       Traits;
  typedef typename Traits::AccessorT      Accessor;
  typedef typename Traits::ValueT         ValueType;
  typedef typename Traits::GridType       GridType;
  typedef typename Traits::GridPtrT       GridPtrType;
  typedef typename Traits::TreeType       TreeType;
  typedef typename Traits::TreePtrT       TreePtrType;


  AccessorWrap(GridPtrType& grid)
  :mAccessor(grid->getAccessor()),
  mMap(*grid){}

  AccessorWrap(const GridType* grid)
  :mAccessor(grid->getAccessor()),
  mMap(*grid){}

  AccessorWrap(TreePtrType& tree)
  :mAccessor(*tree),
  mMap(*openvdb::math::Transform::createLinearTransform(1.0)){}

  AccessorWrap(const TreeType* tree)
  :mAccessor(tree),
  mMap(*openvdb::math::Transform::createLinearTransform(1.0)){}

  AccessorWrap(AccessorWrap* other)
  :mAccessor(other->data()),
  mMap(other->mMap){}

  AccessorWrap copy() const { return *this; }
  void clear(bool){delete this;};
  Accessor& data()
  {
    return mAccessor;
  }

  const Accessor& data() const
  {
    return mAccessor;
  }

  openvdb::Vec3d isGradient(const Coord ijk) const
  {
    return openvdb::math::ISGradient<
                        openvdb::math::CD_2ND>::result
                        ( mAccessor, ijk);
  }

  openvdb::Vec3d isCPT(const Coord ijk) const
  {
    return openvdb::math::CPT<
                        openvdb::math::GenericMap,
                        openvdb::math::CD_2ND>::result
                        ( mMap, mAccessor, ijk);
  }

  openvdb::Vec3d wsCPT(const Coord ijk) const
  {
    return openvdb::math::CPT_RANGE<
                        openvdb::math::GenericMap,
                        openvdb::math::CD_2ND>::result
                        ( mMap, mAccessor, ijk);
  }

  void setValueOn(Coord ijk)
  {
    Traits::setValueOn(mAccessor, ijk);
  }
  void setValueOn(Coord ijk, ValueType val)
  {
    Traits::setValueOn(mAccessor, ijk, val);
  }
  void setValueOnly(Coord ijk, ValueType val)
  {
    Traits::setValueOnly(mAccessor, ijk, val);
  }

  void setValueOff(Coord ijk)
  {
    Traits::setValueOff(mAccessor, ijk);
  }
  void setValueOff(Coord ijk, ValueType val)
  {
    Traits::setValueOff(mAccessor, ijk, val);
  }
private:
  Accessor mAccessor;
  openvdb::math::GenericMap mMap;
}; // class AccessorWrap

} // namespace klAccessor

#endif // KL_ACCESSOR_HAS_BEEN_INCLUDED

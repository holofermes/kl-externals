import os, platform, shutil

system_os = platform.system().lower()

def getFabricVersion(fabricEdkRoot):
  MAJOR_VERSION=''
  MINOR_VERSION=''

  edkHeader = os.path.join(fabricEdkRoot, 'FabricEDK.h')
  f = open(edkHeader, 'r')

  while True:
    line = f.readline().lstrip(' \t')
    if line == "":
      break
    if line.startswith('#define'):
      tokens = line.split()
      if tokens[1] == 'FABRIC_EDK_VERSION_MAJ':
        MAJOR_VERSION = tokens[2]
      elif tokens[1] == 'FABRIC_EDK_VERSION_MIN':
        MINOR_VERSION = tokens[2]
  f.close()

  version = MAJOR_VERSION
  version += '.' + MINOR_VERSION

  return version

def getDirectories(path):
  try:
    return os.walk(path).next()[1]
  except StopIteration:
    return []

def passThroughCmd(target, source, env):
  pass

def removeFolderCmd(target, source, env):
  if os.path.exists(source[0].abspath):
    shutil.rmtree(source[0].abspath)

def which(program):
  def is_exe(fpath):
    return os.path.isfile(fpath) and os.access(fpath, os.X_OK)
  for path in os.environ["PATH"].split(os.pathsep):
    path = path.strip('"')
    exe_file = os.path.join(path, program)
    if platform.system() == 'Windows':
      exe_file += ".exe"
      print exe_file
    if is_exe(exe_file):
      return exe_file
  return None

import os
import sys
import json
from buildUtils import getDirectories, getFabricVersion, system_os, removeFolderCmd, which, passThroughCmd

ALLOWED_MSVC = ('10.0', '12.0', '14.0')
DEFAULT_MSVC = '12.0'
if 'embree' in COMMAND_LINE_TARGETS:
    ALLOWED_MSVC = ('12.0', '14.0')
    DEFAULT_MSVC = ALLOWED_MSVC

variables = Variables('external.py')
variables.AddVariables(
    EnumVariable('MODE', 'Set compiler configuration', 'release', allowed_values=('release', 'debug', 'profile')),
    EnumVariable('MSVC_VERSION', 'Set compiler configuration', DEFAULT_MSVC, allowed_values=(ALLOWED_MSVC)),
    EnumVariable('WARN_LEVEL', 'Set warning level(gcc only)', 'warn-only', allowed_values=('strict', 'warn-only', 'none')),
    EnumVariable('COMPILER', 'Set compiler to use', 'msvc', allowed_values=('msvc', 'gcc')),
    PathVariable('FABRIC_DIR', 'Fabric root', 'PATH/TO/FABRIC'),
    EnumVariable('FABRIC_BUILD_ARCH', 'The architecture you are building for', 'x64_64',  allowed_values=('x86', 'x86_64')),
    PathVariable('INSTALL_PATH', 'Path for installation of extensions', 'install', PathVariable.PathIsDirCreate),
    PathVariable('OPENVDB_INCLUDES', 'OpenVDB includes', '$OPENVDB_ROOT/include', PathVariable.PathAccept),
    PathVariable('OPENVDB_LIB', 'OpenVDB root', '$OPENVDB_ROOT/lib', PathVariable.PathAccept),
    PathVariable('BOOST_INCLUDES', 'Boost includes path', '$BOOST_ROOT/include', PathVariable.PathAccept),
    PathVariable('BOOST_LIB', 'Boost library path', '$BOOST_ROOT/LIB', PathVariable.PathAccept),
    PathVariable('TBB_INCLUDES', 'TBB includes', 'TBB_ROOT/include', PathVariable.PathAccept),
    PathVariable('TBB_LIB', 'TBB library path', '$TBB_ROOT/lib', PathVariable.PathAccept),
    PathVariable('TBB_BIN', 'TBB bin folder. install tbb.dll from this path', '$TBB_ROOT/bin', PathVariable.PathAccept),
    PathVariable('HALF_INCLUDES', 'OpenEXR includes', 'OPENEXR_ROOT/include', PathVariable.PathAccept),
    PathVariable('HALF_LIB', 'OpenEXR library path', '$OPENEXR_ROOT/lib', PathVariable.PathAccept),
    PathVariable('Z_INCLUDES', 'Zlib includes', 'ZLIB_ROOT/include', PathVariable.PathAccept),
    PathVariable('BLOSC_INCLUDES', 'c-blosc includes', 'BLOSC_INCLUDES', PathVariable.PathAccept),
    PathVariable('ZL4_INCLUDES', 'zl4 includes', 'ZL4_INCLUDES', PathVariable.PathAccept),
    PathVariable('Z_LIB_PATH', 'Zlib library path', '$ZLIB_ROOT/lib', PathVariable.PathAccept),
    PathVariable('EXTRA_LIBPATH', 'Extra library path', '$.', PathVariable.PathAccept),
    ('TBB_RELEASE', 'TBB static library', 'tbb'),
    ('TBB_DEBUG', 'TBB static debug library', 'tbb_debug'),
    ('HALF_RELEASE', 'OpemEXR Half static library', 'half'),
    ('HALF_DEBUG', 'OpemEXR Half static debug library', 'halfd'),
    ('Z_LIB', 'Zlib static library', 'z'),
    ('OPENVDB_RELEASE', 'OpenVDB static release library', 'openvdb'),
    ('OPENVDB_DEBUG', 'OpenVDB static debug library', 'openvdbd'),
    PathVariable('EMBREE_INCLUDES', 'Embree root', '$EMBREE_ROOT/include $EMBREE_ROOT/common', PathVariable.PathAccept),
    PathVariable('EMBREE_LIB', 'Embree root', '$EMBREE_ROOT/lib', PathVariable.PathAccept),
    ('EMBREE_RELEASE', 'Embree release libraries', "embree, embree_avx, embree_avx2, embree_sse41, embree_sse42, simd, sys, lexers"),
    ('EMBREE_DEBUG', 'Embree debug libraries', "..."),
    BoolVariable('INSTALL_TBB_DLL', 'Install tbb.dll along with embree.dll.', False),
)

if system_os == 'linux':
  variables.Add(EnumVariable('GCC_VERSION', 'Version of gcc to use', '4.6.3', allowed_values=('4.6.3')))
  env = Environment(variables = variables)
  env.Append(CPPDEFINES = Split('_LINUX'))

# platform defines
#
if system_os == 'darwin':
   env = Environment(variables = variables)
   env.Append(CPPDEFINES = Split('_DARWIN'))
elif system_os == 'windows':
  env = Environment(variables = variables)
  env.Append(CPPDEFINES = Split('_WINDOWS _WIN32 _WIN64 WIN32'))

# run help target and return
# 
if 'help' in COMMAND_LINE_TARGETS:
  help = "external.py variables help:\n\n"
  for option in variables.options:
    current = env[option.key]
    current = "{default}" if current == option.default else current
    help += "%s\n" % option.key
    help += "%s\n" % option.help
    help += "Default: %s \n" % option.default
    help += "Current: %s\n\n" % current
  sys.stdout.write( help )
  env.Alias('help', [env.Command( env.File('help'), "", passThroughCmd )])
  Return()

env['FABRIC_EDK_ROOT'] = os.path.join(env['FABRIC_DIR'], 'include')
env['FABRIC_VERSION']= getFabricVersion(env['FABRIC_EDK_ROOT'])

# msvc settings
#
if env['COMPILER'] == 'msvc':
   #
   if env['WARN_LEVEL'] == 'none':
      env.Append(CCFLAGS = Split('/W0'))
   else:
      env.Append(CCFLAGS = Split('/W1'))
      if env['WARN_LEVEL'] == 'strict':
         env.Append(CCFLAGS = Split(' /WX'))
   env.Append(CCFLAGS = Split('/EHsc /Gd /fp:precise'))
   if env['MODE'] == 'release':
      env.Append(CCFLAGS = Split('/O2 /Oi /Ob2 /MT'))
      env.Append(CPPDEFINES = Split('NDEBUG'))
   elif env['MODE'] == 'profile':
      env.Append(CCFLAGS = Split('/Ob2 /MT /Zi'))
      env.Append(LINKFLAGS = Split('/PROFILE'))
   else:
      env.Append(CCFLAGS = Split('/Od /Zi /MTd'))
      env.Append(LINKFLAGS = Split('/DEBUG'))
      env.Append(CPPDEFINES = Split('_DEBUG'))
# gcc settings
#
elif env['COMPILER'] == 'gcc':

   # warning level
   #
   if env['WARN_LEVEL'] == 'none':
      env.Append(CCFLAGS = Split('-w'))
   else:
      env.Append(CCFLAGS = Split('-Wall -Wsign-compare'))
      if env['WARN_LEVEL'] == 'strict':
         env.Append(CCFLAGS = Split('-Werror'))

   # release flags
   #
   if env['MODE'] == 'release' or env['MODE'] == 'profile':
      env.Append(CCFLAGS = Split('-O3'))

   # debug and profile flags
   #
   if env['MODE'] == 'debug' or env['MODE'] == 'profile':
      if system_os == 'darwin':
         env.Append(CCFLAGS = Split('-gstabs'))
         env.Append(LINKFLAGS = Split('-gstabs'))
      else:
         env.Append(CCFLAGS = Split('-g'))
         env.Append(LINKFLAGS = Split('-g'))

   # linux profiling
   #
   if system_os == 'linux' and env['MODE'] == 'profile':
      env.Append(CCFLAGS = Split('-pg'))
      env.Append(LINKFLAGS = Split('-pg'))

   # mac target arch
   #
   if system_os == 'darwin':
      env.Append(CCFLAGS = Split('-arch x86_64'))
      env.Append(LINKFLAGS = Split('-arch x86_64'))

# configure base directory for temp files
#
if env['COMPILER'] == 'gcc':
    systemBuild = '%s-%s_%s' % (env['COMPILER'], 'gcc'+env['GCC_VERSION'], env['MODE'])
else:
    systemBuild = '%s-%s_%s' % (env['COMPILER'], 'vc'+env['MSVC_VERSION'], env['MODE'])

fabricBuild = 'fe-%s' % env['FABRIC_VERSION']

env['SYSTEM_OS'] = system_os
env['INSTALL_PATH'] = os.path.join(os.path.abspath(env['INSTALL_PATH']), fabricBuild)


rootPath = env.Dir('#').abspath
extsPath = os.path.join(rootPath, 'Exts')
env['EXTS_PATH'] = extsPath

# setup sources for wrapper/s and the clean target
# 
targetsName = ['OpenVDB']
if 'embree' in COMMAND_LINE_TARGETS:
  targetsName += ['embree']
targetsRoot = []
targetsBuild = []
targetHeaders = {}
targetCpps = {}
cleanCmds = []

for name in targetsName:
  root = os.path.join(extsPath, name)
  build = os.path.join('build', system_os, systemBuild, fabricBuild, name)
  headers = '%s/native/include' % root
  cpps = '%s/native/src' % root

  if 'clean' in COMMAND_LINE_TARGETS:
    if os.path.isdir(build):
       cleanCmds.append(env.Command( env.File('cleaning %s build folder' % name), build, removeFolderCmd ))
    if os.path.isdir(headers):
      cleanCmds.append(env.Command( env.File('cleaning %s kl2edk headers' % name), headers, removeFolderCmd ))
    if os.path.isdir(cpps):
      cleanCmds.append(env.Command( env.File('cleaning %s kl2edk sources' % name), cpps, removeFolderCmd ))
    continue

  targetsRoot.append(root)
  targetsBuild.append(build)
  targetHeaders[name] = headers
  targetCpps[name] = cpps

# run the clean target and return
# 
if 'clean' in COMMAND_LINE_TARGETS:
  env.Alias('clean', cleanCmds)
  Return()

# setup kl2edk
# 
kl2edk = which('kl2edk')
if kl2edk is None:
  raise Exception('kl could not be found on the PATH.')

Export('kl2edk')

def RunKL2EDK( env, sources, targetName):
  import subprocess

  headerFolder = targetHeaders[targetName]
  cppFolder = targetCpps[targetName]

  if not os.path.isdir(headerFolder):
    os.makedirs(headerFolder)
  if not os.path.isdir(cppFolder):
    os.makedirs(cppFolder)

  headers=r'--headeroutputfolder=%s' % headerFolder
  cpp=r'--cppoutputfolder=%s' % cppFolder
  extra="--overwrite-cpp-files"
  cmd=" ".join([str(kl2edk),str(sources[0].srcnode().path),str(cpp),str(headers),str(extra)])
  print "[RunKL2EDK]: %s" % cmd
  subprocess.check_call(cmd, shell=True, cwd=rootPath)

  cppFiles = Glob('%s/*.cpp' % cppFolder)
  return cppFiles

env.AddMethod(RunKL2EDK)

targetLibraries = {}
kl2edkResults = {}
# Build the native extensions
#
for root, build, name in zip(targetsRoot, targetsBuild, targetsName):
  env['TARGET_ROOT'] = root
  env['TARGET_NAME'] = name
  targetLibrary, kl2edkResult  = env.SConscript('%s/SConscript' % root,
                                   variant_dir = build,
                                   duplicate = 0,
                                   exports ='env' )
  targetLibraries[name+'Library'] = targetLibrary
  kl2edkResults[name+'kl2edk'] = kl2edkResult


if 'embree' in COMMAND_LINE_TARGETS:
  env.Depends(targetLibraries['embreeLibrary'], kl2edkResults['embreekl2edk'])
env.Depends(targetLibraries['OpenVDBLibrary'], kl2edkResults['OpenVDBkl2edk'])

kl = [kl2edkResults['OpenVDBkl2edk']]
env.Alias('OpenVDB', targetLibraries['OpenVDBLibrary'])
if 'embree' in COMMAND_LINE_TARGETS:
  env.Alias('embree', targetLibraries['embreeLibrary'])
env.Alias('kl', kl)

Default(kl2edkResults['OpenVDBkl2edk'])
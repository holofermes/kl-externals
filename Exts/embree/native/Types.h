#ifndef TYPES_HAS_BEEN_INCLUDED
#define TYPES_HAS_BEEN_INCLUDED



#ifdef WIN32
# define _CRTDBG_MAP_ALLOC
# include <stdlib.h>
# include <crtdbg.h>

# ifdef _DEBUG
#  ifndef DEBUG_NEW
#   define DEBUG_NEW   new( _NORMAL_BLOCK, __FILE__, __LINE__)
#  endif
# else
#  ifndef DEBUG_NEW
#   define DEBUG_NEW
#  endif
# endif
#else
# ifndef DEBUG_NEW
#  define DEBUG_NEW
# endif
#endif  // WIN32


#include <iostream>
#include <algorithm>
#include <FabricEDK.h>
#include "embree2/rtcore.h"
#include "embree2/rtcore_ray.h"
#include "embree2/rtcore_geometry.h"
#include "embree2/rtcore_scene.h"



#include "math/math.h"
#include "math/vec2.h"
#include "math/vec3.h"
#include "math/vec4.h"
#include "math/bbox.h"
#include "math/affinespace.h"

#include "simd/simd.h"


// emrbreekl
#include "include/RTCBoundsFunc.h"

#include "Conversions.h"



class RTCSceneKL
{
public:
  typedef RTCSceneKL Type;

  RTCSceneKL(RTCSceneKL* other)
  :mScene(other->data()){}

  RTCSceneKL(const RTCScene& other)
  :mScene(other){}

  RTCScene& data()
  {
    return mScene;
  }

  const RTCScene& data() const
  {
    return mScene;
  }

  void clear(bool){delete this;};

private:
  RTCScene mScene;
};


// extern "C" void RTCBoundsTo(const RTCBounds* instance,
// extern "C" void RTCBoundsTo(KL::Traits< KL::RTCBoundsFunc >::INParam* instance,
void RTCBoundsTo( const KL::RTCBoundsFunc * instance,
                    size_t item,
                    RTCBounds* bounds_o ){
  EMBREE_TRY_STATEMENT("embree_RTCBoundsTo")
  std::cout << "user instance*: " << instance << std::endl;
  std::cout << "user instance->isValid(): " << instance->isValid() << std::endl;
  std::cout << "with item: " << item << std::endl;

  typedef KL::Traits<KL::UInt32> kl_itemT;
  typedef KL::Traits<KL::RTCBounds> kl_boundsT;

  KL::RTCBounds klBounds;
  std::cout << "conv_to_bounds: " << item << std::endl;
  conv_to_bounds<KL::RTCBounds, RTCBounds>(*bounds_o, klBounds);
  std::cout << "instance->boundsFunc: " << item << std::endl;
  instance->boundsFunc((kl_itemT::INParam)item, (kl_boundsT::IOParam)klBounds);
  std::cout << "conv_from_bounds: " << item << std::endl;
  conv_from_bounds<KL::RTCBounds, RTCBounds>(klBounds, *bounds_o);
  std::cout << "bbox set to: "<< " lower_x:" << bounds_o->lower_x << " lower_y:" << bounds_o->lower_y << " lower_z:" << bounds_o->lower_z << " upper_x:" << bounds_o->upper_x << " upper_y:" << bounds_o->upper_y << " upper_z:" << bounds_o->upper_z << std::endl;
  EMBREE_CATCH_STATEMENT("embree_RTCBoundsTo")
}



void RTCIntersectsTo( const KL::RTCBoundsFunc * instance,
                    RTCRay& ray,
                    size_t item ){

  EMBREE_TRY_STATEMENT("embree_RTCIntersectsTo")
  typedef KL::Traits<KL::UInt32> kl_itemT;
  typedef KL::Traits<KL::RTCRay> kl_rayT;

  KL::RTCRay klRay;
  conv_to_ray<KL::RTCRay, RTCRay>(ray, klRay);

  instance->intersectFunc((kl_itemT::INParam)item, (kl_rayT::IOParam)klRay);
  conv_from_ray<KL::RTCRay, RTCRay>(klRay, ray);

  EMBREE_CATCH_STATEMENT("embree_RTCIntersectsTo")

}


void RTCOccludedTo( const KL::RTCBoundsFunc * instance,
                    RTCRay& ray,
                    size_t item ){

  EMBREE_TRY_STATEMENT("embree_RTCOccludedTo")
  typedef KL::Traits<KL::UInt32> kl_itemT;
  typedef KL::Traits<KL::RTCRay> kl_rayT;

  KL::RTCRay klRay;
  conv_to_ray<KL::RTCRay, RTCRay>(ray, klRay);

  instance->occludedFunc((kl_itemT::INParam)item, (kl_rayT::IOParam)klRay);
  conv_from_ray<KL::RTCRay, RTCRay>(klRay, ray);
  EMBREE_CATCH_STATEMENT("embree_RTCOccludedTo")
}

#endif  //  TYPES_HAS_BEEN_INCLUDED